package si.uni_lj.fri.pbd.navigataioncontrollerexamplekotlin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import si.uni_lj.fri.pbd.navigataioncontrollerexamplekotlin.databinding.MainFragmentBinding


class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.button.setOnClickListener{
            // Navigation.findNavController(it).navigate(R.id.action_mainFragment_to_secondFragment)
            //Navigation.findNavController(it).navigate(MainFragmentDirections.actionMainFragmentToSecondFragment(binding.userText.text.toString()))
        }
    }



}